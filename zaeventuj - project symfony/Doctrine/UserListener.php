<?php

namespace BioBundle\Doctrine;

use Doctrine\ORM\Event\LifecycleEventArgs;
use BioBundle\Entity\UserSettings;
use BioBundle\Entity\User;



/**
 * UserListener listener dla uzytkownika
 * 
 * 
 */
class UserListener 
{
    /**
     * 
     */
    private $em = null;
    
    /**
     * Akcja dla postPersist
     * 
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->em = $args->getEntityManager();
        $entity = $args->getEntity();
     
        if ($entity instanceof User) {
            $this->handleEvent($entity);
        }
    }
    
    /**
     * Obsługa Akcji
     * 
     * @param \BioBundle\Doctrine\User $user
     */
    private function handleEvent(User $user)
    {
        $userSettings = new UserSettings();
        $userSettings->setPostInPage(4);
        $userSettings->setSettingForUser($user);
        
        $this->em->persist($userSettings);
        $this->em->flush();
        
        error_log('dsfds');
    }
}

