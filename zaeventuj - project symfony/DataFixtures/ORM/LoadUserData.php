<?php

namespace BioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use BioBundle\Entity\User;


class LoadUserData  extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $name = function() {
            $nameArray = [
                'Administrator',
                'Użytkownik',
            ];

            return array_rand($nameArray,1);
        };

        for($i = 0; $i < 1; $i++)
        {

            $user = new User();
            $user->setUsername('admin');
            $user->setPassword('$2y$13$exR6CwzeP1F.fpYvGvO2le5RiTkVENM7E5nI2mGIhGLFiaRlFyg5O');
            $user->setIsActive(true);
            $user->setEmail('lucasbozek@gmail.com');

            $manager->persist($user);



        }
        $this->addReference('Admin', $user);
        $manager->flush();

    }
    
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}