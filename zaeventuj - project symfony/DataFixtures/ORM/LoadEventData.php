<?php

namespace BioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use BioBundle\Entity\Event;

class LoadEventData  extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $name = function() {
            $nameArray = [
                'Kurs nurkowania w morzu bałtyckim',
                'Kurs jazdy wyczynowej na wrotkach',
                'Kurs szycia ściegiem francuskim',
                'Kurs spawania metodą MIG/MAG'
            ];

            $rand = array_rand($nameArray,1);
            return $nameArray[$rand];
        };
        
        $category = function() {
            $categoryArray = [
                'IT',
                'Motoryzacja',
                'Gastronomia',
                'Rekodzielo',
                'Hobby'
            ];

            $rand = array_rand($categoryArray,1);
            return $categoryArray[$rand];
        };

        $description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rutrum, erat et gravida dictum, 
            elit ex tristique purus, ac scelerisque diam lacus at eros. Nulla facilisi. Curabitur imperdiet iaculis urna eget pulvinar.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed sem tortor. Phasellus viverra tellus gravida, lacinia libero vitae,
            iaculis nisl. Morbi vestibulum nibh lectus, a tempor quam lobortis nec. Donec maximus ornare tincidunt. Curabitur et mollis purus, 
            sit amet mollis eros. Quisque luctus, est id viverra maximus, ipsum augue sollicitudin turpis, non interdum neque erat eu metus.
            Sed iaculis in odio eget tempor. Mauris eu libero convallis, sagittis augue eu, consequat mauris.'
        ;

        $date = new \DateTime();

        for($i = 0; $i < 20; $i++) {
            $event = new Event();
            $event->setName($name() . " " . $i);
            $event->setDetails($description);
            $event->setTime($date->modify('+1 day') );
            $event->setCategoryA($this->getReference($category()));
            $event->setOwnEventsA($this->getReference('Admin'));
            $event->setLocaction('Mysłowice');
            $manager->persist($event);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}