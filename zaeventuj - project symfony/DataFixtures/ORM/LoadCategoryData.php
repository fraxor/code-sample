<?php
namespace BioBundle\DataFixtures\ORM;
 
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use BioBundle\Entity\Category;
 
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $design = new Category();
    $design->setName('IT');
    $design->setDescription('');
 
    $programming = new Category();
    $programming->setName('Gastronomia');
    $programming->setDescription('Gastronomia');
 
    $manager = new Category();
    $manager->setName('Motoryzacja');
    $manager->setDescription('Motoryzacja');

    $administrator = new Category();
    $administrator->setName('Rekodzieło');
    $administrator->setDescription('Rekodzieło');

    $manager1 = new Category();
    $manager1->setName('Hobby');
    $manager1->setDescription('Hobby');


    $this->addReference('IT', $design);
    $this->addReference('Gastronomia', $programming);
    $this->addReference('Motoryzacja', $manager);
    $this->addReference('Rekodzielo', $administrator);
    $this->addReference('Hobby', $manager1);

    $em->persist($design);
    $em->persist($programming);
    $em->persist($administrator);
    $em->persist($manager);
    $em->persist($manager1);

    $em->flush();


  }
 
  public function getOrder()
  {
    return 1; // the order in which fixtures will be loaded
  }
}

