<?php

namespace BioBundle\Controller;

use BioBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * ReportController odpowiada za drukowanie raportow z eventow
 * 
 * 
 */
class ReportController extends Controller
{
    /**
     * Akcja dla drukowania raportu
     * 
     * @return Response
     */
    public function userEventReportAction()
    {
        $reportManager = $this->container->get('event_report_manager');
        $reports = $reportManager->getUserEventReport();
        
        $response = new Response($reports);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="mojeEventy.csv"');

        return $response;  
    }
    
}

