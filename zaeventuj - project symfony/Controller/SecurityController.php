<?php

namespace BioBundle\Controller;

use BioBundle\Form\Type\RestorePassType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use BioBundle\Form\Type\ChangePassType;

/**
 * SecurityController odpowiada zaautoryzacje uzytkownikow
 * 
 * 
 */
class SecurityController extends Controller
{
    /**
     * Akcja dla logowania
     * 
     * @param Request $request
     * @return type
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        
        $error = $session->get(Security::AUTHENTICATION_ERROR);
        $session->remove(Security::AUTHENTICATION_ERROR);


        return $this->render( 'BioBundle:Security:login.html.twig',array(
            'last_username' => $session->get(Security::LAST_USERNAME),
            'error'         => $error,
        ));
    }
    
    /**
     * Akcja dla wylogowania
     */
    public function LogoutAction()
    {
        $this->addFlash(
            'notice',
            'Wylogowano poprawinie'
        );

        return $this->redirectToRoute("bio_homepage");
    }
    
    /**
     * Akcja dla przenoszenia po wylogowwaniu
     * @return type
     */
    public function afterLogoutAction()
    {
        $this->addFlash(
            'notice',
            'Wylogowano poprawinie'
        );

        return $this->redirectToRoute("bio_homepage");
    }

    /**
     * Akcja dla odzyskiwania hasła. Generuj strone odzyskiwania, obsługuj formularz, wysyła maila
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function restorePassAction(Request $request)
    {
        $form = $this->createForm(RestorePassType::class);
      
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            $emailToRestore =  $form->get('restorepass')->getData();

            $passRestorer = $this->container->get('app.restore_pass');
            $passRestorer->setMailToRestore($emailToRestore);
            $passRestorer->restorePass();

            $this->addFlash('notice', 'wysłano maila');
            return $this->redirectToRoute('bio_homepage', array(), 301);
        }
  
        return $this->render('BioBundle:Pages:restorePass.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    public function changePassAction(Request $request, $key)
    {
        $passResolver = $this->container->get('app.restore_pass');

        $form = $this->createForm(ChangePassType::class, null, array('data' => array('key' => $key)));

        $form->handleRequest($request);
        
        /* Jezeli nie ma keya lub sie nie zgadza to nnie pozwalamy na zmiane hasla */
        if(!$passResolver->checkKey($key) && !$form->isValid()) {
            return $this->redirectToRoute('bio_homepage', array(), 301);
        }

        if($form->isValid()) {

            $passResolver->changePass(
                $form->get('newPassword')->getData(),
                $form->get('key')->getData()
            );

            $this->addFlash('notice', 'Zmieniono hsło');
            return $this->redirectToRoute('bio_homepage', array(), 301);
        }
        
        return $this->render('BioBundle:Pages:changepass.html.twig',array(
            'form' => $form->createView(),
            'key' => $key
        ));
    }
    
}

