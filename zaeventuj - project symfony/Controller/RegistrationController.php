<?php

namespace BioBundle\Controller;

use BioBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use BioBundle\Form\Type\RegisterType;
use BioBundle\Entity\User;

/**
 * RegistrationController odpowiada za rejestracje nowych uzytkownikow
 * 
 * 
 */
class RegistrationController extends Controller
{
    /**
     * Akcja dla rejestracji
     * 
     * @param Request $request
     * @return type
     */
    public function registerAction(Request $request)
    {
        $newUser = new User();
        $form = $this->createForm(RegisterType::class, $newUser);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $form->getData();
            
            $newUser->setPassword($this->encodePassword($user, $user->getplainPassword()));
           

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
           
            $this->authenticateUser($newUser);
            $this->addFlash('notice', 'Pomyslnie utworzono konto');
            
            return $this->redirectToRoute('welcome', array(), 301);
        }

        return $this->render('BioBundle:Registration:register.html.twig', array(
            'form' => $form->createView()
        ));
        
    }
    
    /**
     * Akcja dla wydarzenia po rejestracji
     * 
     * @return type
     */
    public function afterRegisterAction()
    {
        return $this->render('BioBundle:Pages:welcome.html.twig');
    }

    /**
     * 
     * @param User $user
     * @param type $plainPassword
     * @return type
     */
    private function encodePassword(User $user, $plainPassword)
    {
        $encoder = $this->getSecurity('password_encoder');
         
        return $encoder->encodePassword($user, $plainPassword);
    }
    
    private function authenticateUser(User $user)
    {
        $providerKey = 'main';
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->container->get('security.token_storage')->setToken($token);
    }
    
    
    
    
}

