<?php

namespace BioBundle\Controller;

use BioBundle\Form\Type\EventEditType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BioBundle\Entity\Event;
use BioBundle\Form\EventType;
use Doctrine\Common\Collections\Criteria;

/**
 * Event controller.
 *
 */
class EventController extends Controller
{
    /**
     * Lists all Event entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('BioBundle:Event')->findAll();

        return $this->render('event/index.html.twig', array(
            'events' => $events)
        );
    }

    /**
     * Creates a new Event entity.
     *
     */
    public function newAction(Request $request)
    {
        $event = new Event();
        $form = $this->createForm(EventEditType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {



            $em = $this->getDoctrine()->getManager();
            $autor = $em->getRepository('BioBundle:User')->find(1);
            $event->setOwnEventsA($autor);
            $event->setSlug('vcvcv');
            $event->setUpdatedAt(new \DateTime());
            $event->setCreatedAt(new \DateTime());
            $event->setDetails('dziala kurdw');


            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('bio_homepage');
        }

        return $this->render('BioBundle:Pages:newEvent.html.twig', array(
            'event' => $event,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a Event entity.
     *
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BioBundle:Event')->findOneBy(array('slug' => $slug));
        
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('BioBundle:Pages:showEvent.html.twig', array(
            'event' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Event entity.
     *
     */
    public function editAction(Request $request, Event $event)
    {
        /* Sprawdzamy uprawnienia */
        $this->enforceOwnerSecurity($event);

        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm(EventEditType::class, $event);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('event_edit', array(
                'slug' => $event->getSlug()
            ));
        }

        return $this->render('BioBundle:Pages:editEvent.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Event entity.
     *
     */
    public function deleteAction(Request $request, Event $event)
    {
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('event_index');
    }

    /**
     * Creates a form to delete a Event entity.
     *
     * @param Event $event The Event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Zwraca szablon z listą eventow użytkownika
     * 
     * @return type
     */
    public function showUserEventAction() 
    {
	    $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('BioBundle:User')->find($this->getUser());
        $events = $user->getEvents();
        
        return $this->render('BioBundle:Pages:showUserEvents.html.twig', array(
            'events' => $events,
        ));
    }
    
    /**
     * Akcja wykożystywana w Twig do generowania nadchodzących eventów użytkownika
     * 
     * @return type 
     */
    public function _upcomingUserEventsAction() 
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('BioBundle:User')->find($this->getUser());
        $events = $user->getEvents();

        $eventUtils = $this->container->get('app.Event');
        $eventUtils->setUser($this->getUser());
        $timeNow = new \DateTime();

        foreach($events as $event) {

            /* Obejscie. Ograniczenie eventow do tych nadchodzcych. */
            if($event->getTime() < $timeNow ) {
                $events->removeElement($event);     /* Usuwany te, które nie pasują */
            } else {
                $eventUtils->computeExpireDate($event);
            }
        }
        
        return $this->render('BioBundle:Components:userUpcomingEvents.html.twig', 
                array('events' => $events)
        );
        
    }
    
    /**
     * Zwraca listę wszystkich eventów lub eventów użytkownika jako szablon w json
     * 
     * @param type $type
     * @param type $page
     * @return \BioBundle\Controller\Response
     */  
    public function getEventAction(Request $request) 
    {
        $page = $request->request->get('page',0);
        $type = $request->request->get('type', 1);
        $categoryId = $request->request->get('categoryId', 0);

        $eventOnPage = 20;
        if($this->getUser()) {
            $userSettings = $this->getUser()->getUserSettings();
            $eventOnPage = $userSettings->getPostInPage();
        }
        
        $em = $this->getDoctrine()->getManager();
        $eventCount = $em->getRepository('BioBundle:Event')->countEvent(array('dateLimit' => 'now'));

        $pageInDataBase = intdiv($eventCount,$eventOnPage);


        $lastPage = false;
        if($page >= $pageInDataBase) {
            $lastPage = true;
        }
        if($page == 0) {
            $offset = 0;
            $limit = $eventOnPage;
        } else {
            $offset = $page * $eventOnPage+1;
            $limit = ($page+1) * $eventOnPage;
        }
        if($type == 1) {
                $em = $this->getDoctrine()->getManager();

                if($categoryId) {
                    $category = $em->getRepository('BioBundle:Category')->findOneByName($categoryId);
dump($category);
                    $entities = $category->getTopEvent();
                } else {
                    $entities = $em->getRepository('BioBundle:Event')->getAllEventByDate($offset,$limit);
                }

                $eventUtils = $this->container->get('app.Event');
                
                if($this->getUser()) {
                    $eventUtils->setUser($this->getUser());
                }
                foreach($entities as $event) {
                    $eventUtils->computeExpireDate($event);
                }

                $template = $this->render('BioBundle:Components:eventsList.html.twig',array('events'=>$entities))->getContent();
        } else if ($type == 2) {
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('BioBundle:User')->find($this->getUser());
                $events = $user->getEvents();
                error_log('dssss');
                $template = $this->render('BioBundle:Components:userEventsList.html.twig',array('events'=>$events))->getContent();
        }	
	
        $result = array(
            'template' => $template,
            'lastpage' => $lastPage
        );

        $json = json_encode($result);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function _eventByCategoryAction() 
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('BioBundle:Category')->getCategoryWithEvent(1);


        $eventUtils = $this->container->get('app.Event');

        if($this->getUser()) {
            $eventUtils->setUser($this->getUser());
        }

        /* Dodajemy odpowiednie klasy css związane z odległoscią terminu */
        foreach($category as $event) {
            foreach($event->getTopEvent() as $e) {
                $eventUtils->computeExpireDate($e);
            }
        }


        return $this->render('BioBundle:Components:eventByCategory.html.twig', array(
            'categories' => $category)
        );
    }
}
