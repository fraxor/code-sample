<?php

namespace BioBundle\Controller;

use BioBundle\Controller\Controller;

/**
 * UserEventsController
 * 
 * 
 */
class UserEventsController extends Controller
{
    public function attendsAction($slug = 'Super_Event') 
    {   
        $user = $this->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('BioBundle:Event')->findOneBy(array(
            'slug' => $slug)
        );

        $user->addEvent($event);
        $em->persist($user);
        $em->flush();
        
        return $this->redirectToRoute('event_show',array(
            'slug'=>$event->getSlug()
        ));
    }
    public function unAttendsAction($slug = 'Super_Event') 
    {  
        $user = $this->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('BioBundle:Event')->findOneBy(array(
            'slug' => $slug)
        );

        $user->removeEvent($event);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('event_show',array(
            'slug'=>$event->getSlug()
        ));
    }
    
    public function isAttended($id)
    {
        $userEvent = $this->getUser()->getEvents();
        foreach($userEvent as $event) {
            if($event->getId() == $id) {
                return $event;
            }
        }
        return false;
    }
    
    public function _userAttendatnsAction($id = 0) 
    {
        $userEvent = $this->isAttended($id);
        
        if(!$userEvent) {
            $em = $this->getDoctrine()->getManager();
            $userEvent = $em->getRepository('BioBundle:Event')->find($id);

            $userEvent->isNew = true;
        }

        return $this->render('BioBundle:Components:userAttends.html.twig', array(
            'userEvent' => $userEvent
        ));
    }
    
}

