<?php

namespace BioBundle\Controller;

use BioBundle\Form\Type\SettingsType;
use Symfony\Component\HttpFoundation\Request;

/**
 * DefaultController
 * 
 * 
 */
class DefaultController extends Controller
{
    /**
     * Akcja dla strony gównej
     *
     * @return type
     */
    public function indexAction()
    {
        return $this->render('BioBundle:Pages:index2.html.twig', array(
            'index' => true    
            )
        );
    }

    /**
     * Akcja dla strony gównej kategorii
     *
     * @return type
     */
    public function indexCategoryAction($categoryId)
    {
        return $this->render('BioBundle:Pages:index.html.twig', array(
            'categoryId' => $categoryId
            )
        );
    }

    /**
     * Akcja dla strony O nas
     *
     * @return type
     */
    public function aboutAction()
    {
        return $this->render('BioBundle:Pages:about.html.twig');
    }

    /**
     * Akcja dla strony O nas
     * 
     * @return type
     */
    public function contactAction()
    {
        return $this->render('BioBundle:Pages:contact.html.twig');
    }

    /**
     * Akcaj dla polityki prywatności
     *
     * @return type
     */
    public function policyAction()
    {
        return $this->render('BioBundle:Pages:policy.html.twig');
    }

    /**
     * Akcaj dla howto
     * 
     * @return type
     */
    public function howtoAction()
    {
        return $this->render('BioBundle:Pages:howto.html.twig');
    }

    /**
     * Akcja dla Regulaminu
     * 
     * @return type
     */
    public function rulesAction()
    {
        return $this->render('BioBundle:Pages:rules.html.twig');
    }

    /**
     * Akcja dla Ustawien uzytkownika
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function userSettingsAction(Request $request)
    {
        $form = $this->createForm(SettingsType::class,$this->getUser());
      
        $form->handleRequest($request);

        if ($form->isValid()) {

            $user = $form->getData();
         
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('notice', 'Zapisano zmany');
            return $this->redirectToRoute('welcome', array(), 301);
        }
        
        return $this->render('BioBundle:Pages:userSettings.html.twig', array(
            'form' => $form->createView()
        ));
    }
 
}
