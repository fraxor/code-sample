<?php

namespace BioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use BioBundle\Entity\Event;

/**
 * Zmodyfikowany kontroler bazujący na klasie Controller.
 * Wzbogacony o dodatkowe metody.
 * 
 * 
 */
class Controller extends BaseController
{
    /**
     * Zwraca objekt security
     * 
     * @param type $name domyslnie authorization_checker
     * @return type
     */
    public function getSecurity($name = 'authorization_checker')
    {
        return $this->get('security.'.$name);
    }
    
    /**
     * Sprawdza uprawnienia dla posiadacza eventu
     * 
     * @param Event $event
     * @throws type
     */
    public function enforceOwnerSecurity(Event $event)
    {
        $user = $this->getUser();

        if ($user != $event->getownEventsA()) {
            throw $this->createAccessDeniedException('Nie masz prawa edycji!!!');
        }
        return new \Symfony\Component\HttpFoundation\Response();
    }
    
    /**
     * Sprawdza poziom uprawnień użytkownika
     * 
     * @param type $role
     * @throws type
     */
    public function enforceUserSecurity($role = 'ROLE_USER')
    {
        if (!$this->getSecurity()->isGranted($role)) {
            throw $this->createAccessDeniedException('NIe masz uprawnien!');
        }
    }
    
}

