<?php

namespace BioBundle\Utils;

use BioBundle\Entity\Event;
use BioBundle\Entity\User;



/**
 * EventUtils zawiera metody operujące na Eventach
 * 
 * 
 */
class EventUtils 
{
    /**
     *
     * @var type 
     */
    private $user = null;
    
    
    /**
     * Oblicza dni do eventu oraz dodaje odpowiednie klasy w zależności od odległości terminu 
     *      
     * @param Event $event
     * @return Event
     */
    public function computeExpireDate(Event $event)
    {
        $time = $event->getTime();
        $timeNow = new \DateTime();
        $timeNowfirstAlert = clone $timeNow;
        $timeNowSecondAlert = clone $timeNow;
        $timeNowTherdAlert = clone $timeNow;
        
       
        if ($time < $timeNowfirstAlert->modify($this->getAdditionallDate(1))) {
            $class = 'red';
        } else if ($time < $timeNowSecondAlert->modify($this->getAdditionallDate(2))) {
            $class = 'orange';
        } else if ($time < $timeNowTherdAlert->modify($this->getAdditionallDate(3))) {
            $class = 'green';
        } else {
            $class = 'gray';
        }
        
        $event->setCssClass($class);
        return $event;
    }
    
    /**
     * Dodje użytkownika, na którym pracujemy
     * 
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        
    }

    /**
     * Pobiera ustawnienia alertow odnosnie nadchodzacych eventow.
     *
     * @param $type
     * @return string
     */
    private function getAdditionallDate($type)
    {
        if(!$this->user) {
            return $this->getDefaultAdditionalDate($type);
        }
        
        if ($type === 1) {
            return ($this->user->getUserSettings()->getEventAlerts()['1'] != '')?
                $this->user->getUserSettings()->getEventAlerts()['1']:'+0 day';
        } else if ($type === 2) {
            return ($this->user->getUserSettings()->getEventAlerts()['2'] != '')?
                $this->user->getUserSettings()->getEventAlerts()['2']:'+0 day';
        } else if ($type === 3) {
            return ($this->user->getUserSettings()->getEventAlerts()['3'] != '')?
                $this->user->getUserSettings()->getEventAlerts()['3']:'+0 day';
        }
    }

    /**
     * Pobiera domyslna liczbe dni do alertow o nadchodzacych evntach. Jeżeli nie ma zdefiniowanego uytkownika
     *
     * @param $type
     * @return string
     */
    private function getDefaultAdditionalDate($type)
    {
        if ($type === 1) {
            return '+2 day';
        } else if ($type === 2) {
            return '+7 day';
        } else if ($type === 3) {
            return '+1 week';
        }
    }
}

