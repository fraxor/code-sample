<?php

namespace BioBundle\Utils;

use BioBundle\Entity\RestorePass;
use \Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;



/**
 * RestorePass serwis obsługujący odzyskiwanie hasła
 * 
 * 
 */
class RestorePassUtils
{
    /**
     * @var type 
     */
    private $em = null;

    /**
     * @var null|Router
     */
    private $rt = null;

    /**
     * @var null|Template
     */
    private $template = null;

    /**
     * @var Mailer|null
     */
    private $mailer = null;
    /**
     * @var null
     */
    private $email = null;

    /**
     * @var null
     */
    private $key = null;

    /**
     * @var type 
     */
    private $user = null;
    
    
    public function __construct(EntityManager $em, Router $rt, $tp, \Swift_Mailer $mailer, $security)
    {
        $this->em = $em;
        $this->rt = $rt;
        $this->template = $tp;
        $this->mailer = $mailer;
        $this->security = $security;

    }

    /**
     * Ustawia mail, dla ktorego bedzie odzyskiwane hało
     * 
     * @param $mail
     */
    public function setMailToRestore($mail)
    {
        $this->email = $mail;
    }

    /**
     * Odzyskuje haslo dla konta powiazanego z wprowadzonym mailem
     * 
     * @return bool
     */
    public function restorePass()
    {

        if($this->getEmailUser())
        {    
            $this->doRestore();
        }
        return true;
    }

    /**
     * Pobiera uzytkownia, dla ktorego odzyskujemy hasło. Na podstanie maila
     * 
     * @return type
     */
    private function getEmailUser()
    {
        $user = $this->em->getRepository('BioBundle:User')->loadUserByUsername($this->email);
        
        $this->user = $user;
        return $this->user;
    }

    /**
     * Odzyskuje hasło uzytkownika
     * 
     * @return bool
     */
    private function doRestore() 
    {
        if($this->user) {
            $this->prepareRestoreData();
        }
        return true;
    }

    /**
     * Tworzy obiekt odtwarzania hasla. Generuje klucz, ktory identyfikuje i zabezpiecza operaje odzysiwania 
     * 
     */
    private function prepareRestoreData()
    { 
        $secretKey = md5(rand(0,9999).$this->user->getEmail());
        $restore = new RestorePass();
        $restore->setUserId($this->user->getId());
        $restore->setExpireAt((new \DateTime())->modify("+3 hours"));
        $restore->setNewPass($secretKey);
        
        $this->em->persist($restore);
        $this->em->flush();
       
        $this->sendMail($secretKey);

        
    }

    /**
     * Wysyła maila
     * 
     * @param $secretKey
     */
    private function sendMail($secretKey)
    {
        $link =
            $this->rt->generate('change_pass',
            array('key' => $secretKey), UrlGeneratorInterface::ABSOLUTE_URL);

        $message = \Swift_Message::newInstance()
            ->setSubject('Zmiana hasła - Informacja z zaeventuj.pl')
            ->setFrom('zaeventuj@fraxor.nazwa.pl')
            ->setTo('lucasbozek@gmail.com')
            ->setBody(
                $this->template->render('BioBundle:Email:restorepass.html.twig', array('link'=>$link)),'text/html')
        ;

        if($this->mailer->send($message, $failures))
        {
            
        } 
        else {
            error_log('Nie udana proba wyslania maila' . $failures );
        }
    }

    public function setKeyToRestorer($key)
    {
        $this->key = $key;
    }

    public function renderRestoreForm()
    {
        return $this->template->render('BioBundle:Pages:index.html.twig');
    }

    /**
     * Sprawdza czy istnieje obiekt odzyskiwania hasla o podanym keyu
     * 
     * @param $key
     * @return mixed
     */
    public function checkKey($key)
    {
        return $this->em->getRepository('BioBundle:RestorePass')->findByPass($key);
    }

    /**
     * Zmienia haslo. Pobiera użytkownika i zamienia haslo na nowe.
     * 
     * @param $newPass
     * @param $key
     * @return int
     */
    public function changePass($newPass,$key)
    {
        $userFormRestore = $this->checkKey($key);
        $user = $this->em->getRepository('BioBundle:User')->find($userFormRestore->getUserId());

        $user->setPassword($this->security->encodePassword($user, $newPass));

        $this->em->persist($user);
        $this->em->remove($userFormRestore);
        $this->em->flush();

        return 1;
    }
    
}

