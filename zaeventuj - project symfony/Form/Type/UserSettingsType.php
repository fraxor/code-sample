<?php

namespace BioBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use BioBundle\Form\Type\EventAlertsType;


class UserSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('motto')
            ->add('post_in_page'
            );
     
        
        $builder->add('eventAlerts', EventAlertsType::class, array('label'=>'Alert settings')
            );
        
        $builder->get('eventAlerts')
                  ->addModelTransformer(new CallbackTransformer(
                function ($originalDescription) {
                    return $originalDescription;
                },
                function ($submittedDescription) {
                   return $submittedDescription;
                }
            ))
        ;
        
        
        
         
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BioBundle\Entity\UserSettings'
        ));
    }
    
}

