<?php

namespace BioBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class EventAlertsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('1', ChoiceType::class,
                array('choices' => $this->getSelectOptions(),
                      'label' => 'first alert'
            ))
            ->add('2', ChoiceType::class, 
                   array('choices' => $this->getSelectOptions(),
                         'label' => 'second alert'
            ))->add('3', ChoiceType::class, 
                    array('choices' => $this->getSelectOptions(),
                          'label' => 'third alert'
            ));    
    }
    
    private function getSelectOptions() 
    {
        return  array(
                    'empty' => '',
                    '+1 day' => '+1 day',
                    '+2 days' => '+2 day',
                    '+3 days' => '+3 day',
                    '+4 days' => '+4 day',
                    '+1 week' => '+1 week',
                    '+2 weeks' => '+2 week',
                    '+1 month' => '+1 month',
                    '+2 months' => '+2 month'
                );
    }
    
}