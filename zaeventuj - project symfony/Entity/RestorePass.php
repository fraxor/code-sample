<?php

namespace BioBundle\Entity;

/**
 * RestorePass
 */
class RestorePass
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var string
     */
    private $newPass;

    /**
     * @var \DateTime
     */
    private $expire_at;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return RestorePass
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set newPass
     *
     * @param string $newPass
     *
     * @return RestorePass
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;

        return $this;
    }

    /**
     * Get newPass
     *
     * @return string
     */
    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * Set expireAt
     *
     * @param \DateTime $expireAt
     *
     * @return RestorePass
     */
    public function setExpireAt($expireAt)
    {
        $this->expire_at = $expireAt;

        return $this;
    }

    /**
     * Get expireAt
     *
     * @return \DateTime
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }
}
