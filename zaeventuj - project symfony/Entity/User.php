<?php

namespace BioBundle\Entity;


use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Serializable;
use Doctrine\Common\Collections\Criteria;


/**
 * User
 */
class User implements AdvancedUserInterface, Serializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $is_active = true;
    
    /**
     * @var array
     */
    private $roles = array();

    /**
     * @var string
     */
    private $plainPassword = null;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->isActive = true;
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }
    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
    
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function eraseCredentials()
    {
        $this->setPlainPassword(null);
    }

  
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }


    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }
    
    public function getRoles()
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        
        return array_unique($roles);
    }
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return true;
    }
    
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        
        if($plainPassword) {
            $this->plainPassword = $plainPassword;
        }

        return $this;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;


    /**
     * Add event
     *
     * @param \BioBundle\Entity\Event $event
     *
     * @return User
     */
    public function addEvent(\BioBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \BioBundle\Entity\Event $event
     */
    public function removeEvent(\BioBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $authorA;


    /**
     * Add authorA
     *
     * @param \BioBundle\Entity\User $authorA
     *
     * @return User
     */
    public function addAuthorA(\BioBundle\Entity\User $authorA)
    {
        $this->authorA[] = $authorA;

        return $this;
    }

    /**
     * Remove authorA
     *
     * @param \BioBundle\Entity\User $authorA
     */
    public function removeAuthorA(\BioBundle\Entity\User $authorA)
    {
        $this->authorA->removeElement($authorA);
    }

    /**
     * Get authorA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorA()
    {
        return $this->authorA;
    }
    
    /**
     *
     * @return type
     */
    public function __toString()
    {
        return (string)$this->getUsername();
    }
    /**
     * @var \BioBundle\Entity\UserSettings
     */
    private $userSettings;


    /**
     * Set userSettings
     *
     * @param \BioBundle\Entity\UserSettings $userSettings
     *
     * @return User
     */
    public function setUserSettings(\BioBundle\Entity\UserSettings $userSettings = null)
    {
        $this->userSettings = $userSettings;

        return $this;
    }

    /**
     * Get userSettings
     *
     * @return \BioBundle\Entity\UserSettings
     */
    public function getUserSettings()
    {
        return $this->userSettings;
    }

}
