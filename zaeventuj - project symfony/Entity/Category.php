<?php


namespace BioBundle\Entity;

use Doctrine\Common\Collections\Criteria;


/**
 * Category
 */
class Category
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
	/**
	 * Konwersja Obiektu do Stringa
	 * 
	 * @return type
	 */
    public function __toString()
    {
      return $this->getName();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventsA;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->eventsA = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Add eventsA
     *
     * @param \BioBundle\Entity\Event $eventsA
     *
     * @return Category
     */
    public function addEventsA(\BioBundle\Entity\Event $eventsA)
    {
        $this->eventsA[] = $eventsA;

        return $this;
    }

    /**
     * Remove eventsA
     *
     * @param \BioBundle\Entity\Event $eventsA
     */
    public function removeEventsA(\BioBundle\Entity\Event $eventsA)
    {
        $this->eventsA->removeElement($eventsA);
    }

    /**
     * Get eventsA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventsA()
    {
        return $this->eventsA;
    }
    
    public function getTopEvent($limit = null)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->gte("time", new \DateTime()))
            ->orderBy(array("time" => Criteria::ASC))
            ->setFirstResult(0);
        if($limit) {
            $criteria->setMaxResults(5);
        }

        $event = $this->getEventsA()->matching($criteria);

        return $this->eventsA = $event;
    }
}
