<?php

namespace BioBundle\Entity;

/**
 * Event
 */
class Event
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $time;

    /**
     * @var string
     */
    private $locaction;

    /**
     * @var string
     */
    private $details;

    /**
     * @var string Klasa css, zwiazana z liczba dni do eventu
     */
    private $cssClass = '';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Event
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set locaction
     *
     * @param string $locaction
     *
     * @return Event
     */
    public function setLocaction($locaction)
    {
        $this->locaction = $locaction;

        return $this;
    }

    /**
     * Get locaction
     *
     * @return string
     */
    public function getLocaction()
    {
        return $this->locaction;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return Event
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \BioBundle\Entity\User $user
     *
     * @return Event
     */
    public function addUser(\BioBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \BioBundle\Entity\User $user
     */
    public function removeUser(\BioBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * @var integer
     */
    private $author_id;

    /**
     * @var \BioBundle\Entity\User
     */
    private $ownEventsA;


    /**
     * Set authorId
     *
     * @param integer $authorId
     *
     * @return Event
     */
    public function setAuthorId($authorId)
    {
        $this->author_id = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * Set ownEventsA
     *
     * @param \BioBundle\Entity\User $ownEventsA
     *
     * @return Event
     */
    public function setOwnEventsA(\BioBundle\Entity\User $ownEventsA = null)
    {
        $this->ownEventsA = $ownEventsA;

        return $this;
    }

    /**
     * Get ownEventsA
     *
     * @return \BioBundle\Entity\User
     */
    public function getOwnEventsA()
    {
        return $this->ownEventsA;
    }
    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $indexes;


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Event
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set indexes
     *
     * @param string $indexes
     *
     * @return Event
     */
    public function setIndexes($indexes)
    {
        $this->indexes = $indexes;

        return $this;
    }

    /**
     * Get indexes
     *
     * @return string
     */
    public function getIndexes()
    {
        return $this->indexes;
    }
    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;


 

    /**
     * @var string
     */
    private $lifecycleCallbacks;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set lifecycleCallbacks
     *
     * @param string $lifecycleCallbacks
     *
     * @return Event
     */
    public function setLifecycleCallbacks($lifecycleCallbacks)
    {
        $this->lifecycleCallbacks = $lifecycleCallbacks;

        return $this;
    }

    /**
     * Get lifecycleCallbacks
     *
     * @return string
     */
    public function getLifecycleCallbacks()
    {
        return $this->lifecycleCallbacks;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }
    /**
     * @var integer
     */
    private $category_id;


    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return Event
     */
    public function setCategoryId($categoryId)
    {
        $this->category_id = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }
    /**
     * @var \BioBundle\Entity\Category
     */
    private $categoryA;


    /**
     * Set categoryA
     *
     * @param \BioBundle\Entity\Category $categoryA
     *
     * @return Event
     */
    public function setCategoryA(\BioBundle\Entity\Category $categoryA = null)
    {
        $this->categoryA = $categoryA;

        return $this;
    }

    /**
     * Get categoryA
     *
     * @return \BioBundle\Entity\Category
     */
    public function getCategoryA()
    {
        return $this->categoryA;
    }

    public function setCssClass($class)
    {
        $this->cssClass = $class;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }
    
}
