<?php

namespace BioBundle\Entity;


/**
 * UserSettings
 */
class UserSettings
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $post_in_page;

    /**
     * @var \BioBundle\Entity\User
     */
    private $settingsForUser;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserSettings
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set postInPage
     *
     * @param integer $postInPage
     *
     * @return UserSettings
     */
    public function setPostInPage($postInPage)
    {
        $this->post_in_page = $postInPage;

        return $this;
    }

    /**
     * Get postInPage
     *
     * @return integer
     */
    public function getPostInPage()
    {
        return $this->post_in_page;
    }
    
    /**
     * @var \BioBundle\Entity\User
     */
    private $settingForUser;


    /**
     * Set settingForUser
     *
     * @param \BioBundle\Entity\User $settingForUser
     *
     * @return UserSettings
     */
    public function setSettingForUser(\BioBundle\Entity\User $settingForUser = null)
    {
        $this->settingForUser = $settingForUser;

        return $this;
    }

    /**
     * Get settingForUser
     *
     * @return \BioBundle\Entity\User
     */
    public function getSettingForUser()
    {
        return $this->settingForUser;
    }
    /**
     * @var string
     */
    private $motto;


    /**
     * Set motto
     *
     * @param string $motto
     *
     * @return UserSettings
     */
    public function setMotto($motto)
    {
        $this->motto = $motto;

        return $this;
    }

    /**
     * Get motto
     *
     * @return string
     */
    public function getMotto()
    {
        return $this->motto;
    }
    /**
     * @var array
     */
    private $event_alerts;


    /**
     * Set eventAlerts
     *
     * @param array $eventAlerts
     *
     * @return UserSettings
     */
    public function setEventAlerts($eventAlerts)
    {
        $this->event_alerts = $eventAlerts;

        return $this;
    }

    /**
     * Get eventAlerts
     *
     * @return array
     */
    public function getEventAlerts()
    {
        if(empty($this->event_alerts)) {
            return array(
                '1' => '',
                '2' => '',
                '3' => ''
            );
        }
        return $this->event_alerts;
    }
}
