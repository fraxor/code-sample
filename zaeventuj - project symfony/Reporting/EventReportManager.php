<?php

namespace BioBundle\Reporting;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class EventReportManager 
{
    /**
     *
     * @var type 
     */
    private $em = null;
    
    /**
     *
     * @var type 
     */
    private $router = null;
    
    
    public function __construct($em, $rt) {
        $this->em = $em ;
        $this->router = $rt ;
    }
    public function getUserEventReport()
    {   
        $events = $this->em->getRepository('BioBundle:Event')->getAllUserEventByDate();
        
        $rows = array();
        foreach ($events as $event) {
            $data = array($event->getId(), 
                $event->getName(), 
                $event->getTime()->format('y-M-d H:m:s'), 
                $this->router->generate('event_show', 
                        array('slug' => $event->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL));

            $rows[] = implode(';', $data);
        }

        return implode("\n", $rows);
        
    }
    
}

