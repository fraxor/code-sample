<?php

/**
 * Interface UserInterface
 */
interface UserInterface
{
    public function setName(string $name) : UserInterface;
    public function setAge(int $age) : UserInterface;
    public function setAddress(string $address) : UserInterface;
    public function getName() : string;
    public function getAge() : int;
    public function getAddress() : string;
}

/**
 * Class User
 * Klasa bazowa rozszerzana przez wyspecjalizowane
 *
 */
class User implements UserInterface
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $age = '';

    /**
     * @var string
     */
    protected $address = '';

    /**
     * User constructor.
     * @param string $name
     * @param int $age
     * @param string $address
     */
    public function __construct(string $name, int $age, string $address)
    {
        $this->setName($name);
        $this->setAge($age);
        $this->setAddress($address);
    }

    /**
     * @param string $name
     * @return UserInterface
     */
    public function setName(string $name) : UserInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param int $age
     * @return UserInterface
     */
    public function setAge(int $age) : UserInterface
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @param string $address
     * @return UserInterface
     */
    public function setAddress(string $address) : UserInterface
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge() : int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getAddress() : string
    {
        return $this->address;
    }

}


/**
 * Class vipUser
 */
class vipUser extends User
{
    /**
     * @var int
     */
    private $vipLevel = 0;

    /**
     * vipUser constructor.
     * @param string $name
     * @param int $age
     * @param string $address
     */
    public function __construct(string $name, int $age, string $address)
    {
        parent::__construct($name, $age, $address);
        $this->setVipLevel(1);
    }

    /**
     * @param int $level
     * @return vipUser
     */
    public function setVipLevel(int $level) : vipUser
    {
        $this->vipLevel = $level;
        return $this;
    }

    /**
     * @return int
     */
    public function getVipLevel() : int
    {
        return $this->vipLevel;
    }

}


/**
 * Interface LuckyNameLotteryInterface
 */
interface LuckyNameLotteryInterface
{
    public function getLotteryName() : string;
    public function checkWin(string $name) : bool;
    public function getPrize() : string;
}

/**
 * Class LuckyNameLottery
 */
class LuckyNameLottery implements LuckyNameLotteryInterface
{
    /**
     * @var array|null
     */
    private $luckyName = null;

    /**
     * @var null|string|type
     */
    private $prize = null;

    /**
     * @var int
     */
    private static $winCount = 0;

    /**
     * LuckyNameLottery constructor.
     * @param array $nameArray
     * @param string $prize
     */
    public function __construct(array $nameArray, string $prize)
    {
        $this->luckyName = $nameArray;
        $this->prize = $prize;
    }

    /**
     * @return string
     */
    public function getLotteryName()  : string
    {
        return LuckyNameLottery::class;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function checkWin(string $name) : bool
    {
        $win = false;
        $cal = function($key) use ($name, &$win) {
            /* to imie jest na liscie, wiec uzytkownik wygrywa */
            if($name == $key ) {
                $win = true;
                self::$winCount++;
            }
        };

        array_walk($this->luckyName,$cal);

        /* w tej wersji wygrywają wszystkie osoby o takiem imieniu, a nie tylko pierwsza */
        if(0) {
            if(($key = array_search($name, $this->luckyName)) !== false) {
                unset($this->luckyName[$key]);
            }
        }

        return $win;

    }

    /**
     * @return int
     */
    public static function getWinCount() : int
    {
        return static::$winCount;
    }

    /**
     * @return string
     */
    public function getPrize() : string
    {
        return $this->prize;
    }

}

/**
 * ShippingInterface
 *
 */
interface ShippingInterface
{
    public function send() : bool ;
    public function receiver(User $user) : ShippingInterface;
    public function item(string $prize) : ShippingInterface;
}

/**
 * LotryShipping klasa obslugujaca dostawe nagrody. TODO
 *
 */
class LotteryShipping implements ShippingInterface
{
    /**
     * @var null
     */
    private $receiver = null;

    /**
     * @var null
     */
    private $item = null;

    /**
     * LotteryShipping constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return bool
     */
    public function send() : bool
    {
        return true;
    }

    /**
     * @param string $item
     * @return ShippingInterface
     */
    public function item(string $item) : ShippingInterface
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @param User $user
     * @return ShippingInterface
     */
    public function receiver(User $user) : ShippingInterface
    {
        $this->receiver = $user;
        return $this;
    }

}

/**
 * LotteryUtil klasa obslugujaca przebieg loterii
 *
 */
class LotteryUtil
{
    /**
     * @var LuckyNameLotteryInterface|null
     */
    private $lottery = null;

    /**
     * @var null|ShippingInterface
     */
    private $shipping = null;

    /**
     * LotteryUtil constructor.
     * @param LuckyNameLotteryInterface $lottery
     * @param ShippingInterface $shipping
     */
    public function __construct(LuckyNameLotteryInterface $lottery, ShippingInterface $shipping)
    {
        $this->lottery = $lottery;
        $this->shipping = $shipping;
    }

    /**
     * Sprawdza czy uzytkownik wygral. Jezeli wygral to wysyla nagrode
     * i dopisuje do imienia uzytkownika odznake zwyciescy.
     *
     * @param UserInterface $user
     * @return LotteryUtil
     */
    public function checkForWinner(UserInterface $user) : LotteryUtil
    {
        $isWinner = $this->lottery->checkWin($user->getName());

        if($isWinner) {
            $user->setName($user->getName() . ' ' . ' (LuckyName Winner!)');
            $this->sendPrize($user);
        }

        return $this;
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function sendPrize(UserInterface $user)  : bool
    {
        $this->shipping->receiver($user);
        $this->shipping->item($this->lottery->getPrize());

        if($this->shipping->send()) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * Fabryka userow.
 * Generuje zadana liczbe userow o losowym imienu, wieku i adresie
 *
 */
class UserGenerator
{
    /**
     * @var array
     */
    private $users = array();

    /**
     * @var int|null
     */
    private $amount = null;

    /**
     * @var array
     */
    private $name = [
        'Jan','Małgorzata','Piotr','Anna','Łukasz','Maurycy','Wawrzyniec','Felicja','Igor','Malwina','Natalia',
        'Julia','Karolina','Tomek','Ola', 'Agnieszka', 'Miłosz', 'Franek', 'Ala', 'Nemo', 'Anna'
    ];

    /**
     * @var array
     */
    private $address = [
        'Klonowa','Jod','ulowa','Wrzosowa','ilorazowa','nowa','Reks','Gorna','Brzydka','Twarda','Lol',
        'Rotacja','Tata','Pole','Komputer'
    ];

    /**
     * UserGenerator constructor.
     * @param int $amount
     */
    public function __construct(int $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return array
     */
    public function generate() : array
    {
        /* tworzymy tylu userow ilu zadano */
        for($i = 0; $i < $this->amount; $i++) {
            $randName = array_rand($this->name);
            $randAddress = array_rand($this->address);
            $this->users[] = new User($this->name[$randName], rand(15,99), $this->address[$randAddress]);
        }
        return $this->users;
    }

}



/* generujemy uzytkowinikow */
$userAmount = 90000;
$users = (new UserGenerator($userAmount))
    ->generate();

/* ustawiamy szczęsliwe imiona */
$luckyName = [
    'Anna'
];

/* inicjujemy gre szczesliwymi imionami. TODO generowanie zwycieskich imion */
$lottery = new LuckyNameLottery($luckyName, 'Kajak');

/* przygotowanie systemu obslugi loterii */
$lotteryMachine = new LotteryUtil($lottery, new LotteryShipping());

foreach($users as $user) {
    $lotteryMachine->checkForWinner($user);
}

/* TODO maszyna drukuje komunikat */
echo "W loterii ** " . $lottery->getLotteryName() . " ** padło " . LuckyNameLottery::getWinCount() . " wygranych! ";
echo "Liczba osób biorących udział w zabawie: " . $userAmount;

