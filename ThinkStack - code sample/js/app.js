/*
 * Copyright (c) 2016.  
 */

$(document).ready(function() {

    app.getEntry();

    $("#addEvent").bind("click", function() {
        
        if(!$('#entry').val()) {
            alert('Należy uzupełnić pole "Wprowadź wpis"');
            $('#entry').focus();
            return false;
        }
        
        app.addEntry($('#entry').val());
        $('#entry').val('');
        $('#entry').focusout();
    });
   
}).keypress(function(e){
    if (e.which === 13){
        $("#addEvent").click();
    }
});


/* Obiekt aplikacji */
var app = app || {
    
    ajaxActionLink : "app/Controller/ajaxActions.php",
    
    domAction : {
        addToDOM : function(val) {
             var elem = $(
                 '<tr>' +
                 '<td>'+ val.name + '</td>'+
                ' <td>'+ val.date + '</td>'+
                '</tr>'
             ).hide();

            $('#appData').append(elem);
            elem.show("slow");
        }
    },
    add : function(post) {

        $.each(post,function () {

            app.domAction.addToDOM(this);
        });
    },
    addEntry : function(val) {
        $.ajax({
            method: "POST",
            url: this.ajaxActionLink,
            data: { action: "addEntry", name: val }
        }).done(function( data ) {
            var elem = JSON.parse( data );

            if(elem) {
                var fo = $(
                    '<tr>' +
                    '<td>'+ elem.name + '</td>'+
                    '<td>'+ elem.date.date.slice(0, 19) + '</td>'+
                    '</tr>'
                ).hide();

                $('#appData tr:first-child').before(fo);
                fo.show("slow");
            }
        });
    },
    getEntry : function() {
        $.ajax({
            method: "POST",
            url: this.ajaxActionLink,
            data: { action: "getEntry" }
        }).done(function( data ) {
            var entries = JSON.parse( data );

            app.add(entries);
        });
    }
};

