<?php
/**
 * Created by Łukasz Bożek.
 * Date: 2015-2016
 *
 */
require_once '../bootstrap/autoload.php';

$action = filter_input(INPUT_POST, 'action');
$newEntryContent = filter_input(INPUT_POST, 'name');
$entry = '';

$app = new ThinkStack\Component\App\AppCore\App();

switch ($action)
{
    case 'getEntry':
        
        $entry = $app->getEntry();

        if(!empty($entry)) {
            $app->getResponse()->setStatus(ThinkStack\Component\Response\Response::RESPONSE_STATUS_OK);
            $app->getResponse()->setData($entry);
        }

        print $app->getResponse()->getJsonResponse();

        break;

    case 'addEntry':
        
        $entry = $app->addEntry($newEntryContent);

        if(!empty($entry)) {
            $app->getResponse()->setStatus(ThinkStack\Component\Response\Response::RESPONSE_STATUS_OK);
            $app->getResponse()->setData($entry);
        }

        print $app->getResponse()->getJsonResponse();

        break;

}

exit();
    
