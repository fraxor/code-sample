<?php
/**
 * Created by Łukasz Bożek.
 * Date: 2015-2016
 *
 */

namespace ThinkStack\Component\App\AppCore;

use ThinkStack\Component\Entry\Entry;
use ThinkStack\Component\Response\Response;

/**
 * Class App
 * @package ThinkStack\Component\App\AppCore
 */
class App
{
    /**
     * @var Entry
     */
    private $entry;

    /**
     * @var Response
     */
    private $response;

    /**
     * App constructor.
     */
    public function __construct()
    {
        $this->entry = new Entry();
        $this->response = new Response();

    }

    /**
     * Pobiera wszystkie wpisy
     *
     * @return mixed
     */
    public function getEntry()
    {
        return $this->entry->getEntry();
    }

    /**
     * Dodaje wpis
     *
     * @param $name
     * @return array
     */
    public function addEntry($name)
    {
        return $this->entry->addEntry($name);
    }

    /**
     * Pobiera obiekt odpowiedzi
     *
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
    
}