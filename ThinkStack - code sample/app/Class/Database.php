<?php
/**
 * Created by Łukasz Bożek.
 * Date: 2015-2016
 *
 */

namespace ThinkStack\Component\Database;

/**
 * Class Database
 * Klasa obslugujaca opeacje na bazie danych
 *
 */
class Database implements DatabaseInterface
{

    /* Dane do polaczenia */
    private $host     = '127.0.0.1';
    private $user     = 'root';
    private $pass     = '';
    private $dbname   = 'test';

    private $dbh             =  null;
    private $error           =  null;
    private $stmt            =  null;
    private static $instance  =  null;

    public function __construct()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        $options = array(
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        );

        try{
            $this->dbh = new \PDO($dsn, $this->user, $this->pass, $options);

        } catch(\PDOException $e){
            $this->error = $e->getMessage();
        }
    }

    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __clone()
    {
    }

    public function query($query)
    {
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                    break;
                default:
                    $type = \PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function result()
    {
        $this->execute();
        return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}
