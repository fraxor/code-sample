<?php

namespace ThinkStack\Component\Response;

/**
 * Class Response
 * @package ThinkStack\Component\Response
 */
class Response implements ResponseInterface 
{
    /**
     *
     */
    const RESPONSE_STATUS_OK = 1;

    /**
     *
     */
    const RESPONSE_STATUS_ERROR = 0;

    /**
     * Dane odpowiedzi
     *
     * @var null
     */
    private $data = null;

    /**
     * Status odpowiedzi
     *
     * @var null
     */
    private $status = null;

    /**
     * Response constructor.
     */
    public function __construct() 
    {
    }

    /**
     * @return null
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status) 
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data) 
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return null
     */
    public function getData() 
    {
        return $this->data;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getValue($key) 
    {
        if(key_exists($key, $this->data)){
            return $this->data[$key];
        }
        
    }
    
    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setValue($key, $value) 
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJsonResponse()
    {
        return json_encode($this->getData());
    }

}
