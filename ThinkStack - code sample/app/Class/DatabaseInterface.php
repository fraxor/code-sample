<?php
/**
 * Created by Łukasz Bożek.
 * Date: 2015-2016
 *
 */

namespace ThinkStack\Component\Database;

/**
 * Interface DatabaseInterface
 */
interface DatabaseInterface
{
    public function query($query);
    public function bind($param, $value, $type = null);
    public function execute();
    public function result();
}