<?php

namespace ThinkStack\Component\Response;

interface ResponseInterface 
{
    public function setStatus($status);
    public function getStatus();
    public function setValue($key, $value);
    public function getValue($key);
    public function setData($data);
    public function getData();
}
