<?php
/**
 * Created by Łukasz Bożek.
 * Date: 2015-2016
 *
 */

namespace ThinkStack\Component\Entry;

use ThinkStack\Component\Database\Database;

/**
 * Klasa wpisu
 */
class Entry
{
    /**
     * Entry constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getEntry()
    {
        $db = Database::getInstance();
        $db->query('SELECT name,date FROM entry order by date DESC');
        $res = $db->result();
 
        return $res;
    }

    /**
     * @param $name
     * @return array
     */
    public function addEntry($name)
    {
        $db = Database::getInstance();
        $db->query("INSERT INTO `entry` (`name`, `date`) VALUES ('{$name}', NOW())");
        $res = $db->execute();

        if($res) {
            $res = array();
            $res['name'] = $name;
            $res['date'] = new \DateTime('now');
        }
        
        return $res;
    }
}