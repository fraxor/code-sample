<?php
/**
 * Created by Łukasz Bożek.
 * Date: 2015-2016
 *
 */

spl_autoload_register(function ($class) {
    $parts = explode('\\', $class);
    require '../Class/' . end($parts) . '.php';
});
